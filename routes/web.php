<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//admin parents route
Route::prefix('admin')->group(function () {
Route::get('login', 'AdminController@login_page')->name('login_page');
Route::get('Dashboard', 'SuperAdminController@superAdminDashboard')->name('superAdminDashboard');
Route::post('check_login', 'AdminController@check_login')->name('check_login');
Route::get('superAdminLogout', 'SuperAdminController@superAdminLogout')->name('superAdminLogout');
//admin employee route
Route::get('Employee', 'SuperAdminController@addEmployee')->name('addEmployee');
Route::post('Insert-Employee-Data', 'SuperAdminController@insertEmployeeData')->name('insertEmployeeData');
});

//employee parent route
Route::prefix('employee')->group(function () {
Route::get('employee-login', 'EmployeeController@employee_login_page')->name('employee_login_page');
Route::get('Employee-Dashboard', 'SuperEmployeeController@superEmployeeDashboard')->name('superEmployeeDashboard');
Route::post('employee_check_login', 'EmployeeController@employee_check_login')->name('employee_check_login');
Route::get('superEmployeeLogout', 'SuperEmployeeController@superEmployeeLogout')->name('superEmployeeLogout');
Route::post('update-employee-information', 'SuperEmployeeController@updateEmployeeInformation')->name('updateEmployeeInformation');
Route::get('employee-attendance', 'SuperEmployeeController@employeeAttendance')->name('employeeAttendance');
Route::post('insert-employee-attendance', 'SuperEmployeeController@insertEmployeeAttendance')->name('insertEmployeeAttendance');
});