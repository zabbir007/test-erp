<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@yield('title','Admin Dashboard')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="_base_url" content="{{ url('/') }}">

        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('admin/assets/images/favicon.ico')}}">

        
        <!-- Plugins css  for image upload file -->


         <!-- Sweet Alert-->
        <link href="{{asset('admin/assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />

          <!--Date css-->
          <!-- Plugins css -->
        <link href="{{asset('admin/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/clockpicker/bootstrap-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!-- Plugins css  for select option-->
       <!-- Plugins css -->
        <link href="{{asset('admin/assets/libs/jquery-nice-select/nice-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css')}}" rel="stylesheet" type="text/css" />
        <!-- End Plugins css  for select option-->

        <!-- Plugins css -->
        <link href="{{asset('admin/assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- Plugins css  for image upload file-->

        <!-- Plugins css -->
        <link href="{{asset('admin/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />

        

        <!-- third party data-table css -->
        <link href="{{asset('admin/assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party data-table css end -->

        <!-- App css -->
        <link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        

        


    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">
            <?php
            $route=Route::currentRouteName();
            if($route!="login_page"){
               
            ?>
            <!-- Topbar Start -->
            @include('includes.admin.headerNavigation')
            
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->

             @include('includes.admin.headerSidebar')
            <?php
                }
             ?>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">
                            @yield('content')
                  </div> <!-- container -->

                </div> <!-- content -->

                <!-- Footer Start -->
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                2020 @ Developed By  <a href="#">Demo</a> 
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right footer-links d-none d-sm-block">
                                    <a href="javascript:void(0);">About Us</a>
                                    <a href="javascript:void(0);">Help</a>
                                    <a href="javascript:void(0);">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            <div class="rightbar-title">
                <a href="javascript:void(0);" class="right-bar-toggle float-right">
                    <i class="dripicons-cross noti-icon"></i>
                </a>
                <h5 class="m-0 text-white">Settings</h5>
            </div>
            <div class="slimscroll-menu">
                <!-- User box -->
                <div class="user-box">
                    <div class="user-img">
                        <img src="{{asset('admin/assets/images/users/user-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        <a href="javascript:void(0);" class="user-edit"><i class="mdi mdi-pencil"></i></a>
                    </div>
            
                    <h5><a href="javascript: void(0);">Geneva Kennedy</a> </h5>
                    <p class="text-muted mb-0"><small>Admin Head</small></p>
                </div>

                <!-- Settings -->
                <hr class="mt-0" />
                <h5 class="pl-3">Basic Settings</h5>
                <hr class="mb-0" />

                <div class="p-3">
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox1" type="checkbox" checked>
                        <label for="Rcheckbox1">
                            Notifications
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox2" type="checkbox" checked>
                        <label for="Rcheckbox2">
                            API Access
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox3" type="checkbox">
                        <label for="Rcheckbox3">
                            Auto Updates
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-2">
                        <input id="Rcheckbox4" type="checkbox" checked>
                        <label for="Rcheckbox4">
                            Online Status
                        </label>
                    </div>
                    <div class="checkbox checkbox-primary mb-0">
                        <input id="Rcheckbox5" type="checkbox" checked>
                        <label for="Rcheckbox5">
                            Auto Payout
                        </label>
                    </div>
                </div>

                <!-- Timeline -->
                <hr class="mt-0" />
                <h5 class="pl-3 pr-3">Messages <span class="float-right badge badge-pill badge-danger">25</span></h5>
                <hr class="mb-0" />
                <div class="p-3">
                    <div class="inbox-widget">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{asset('admin/assets/images/users/user-2.jpg')}}" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Tomaslau</a></p>
                            <p class="inbox-item-text">I've finished it! See you so...</p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{asset('admin/assets/images/users/user-3.jpg')}}" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Stillnotdavid</a></p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{asset('admin/assets/images/users/user-4.jpg')}}" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Kurafire</a></p>
                            <p class="inbox-item-text">Nice to meet you</p>
                        </div>

                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{asset('admin/assets/images/users/user-5.jpg')}}" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Shahedk</a></p>
                            <p class="inbox-item-text">Hey! there I'm available...</p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{asset('admin/assets/images/users/user-6.jpg')}}" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author"><a href="javascript: void(0);" class="text-light">Adhamdannaway</a></p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                        </div>
                    </div> <!-- end inbox-widget -->
                </div> <!-- end .p-3-->

            </div> <!-- end slimscroll-menu-->
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>


        <!-- Vendor js -->
        <script src="{{asset('admin/assets/js/vendor.min.js')}}"></script>

        <!-- Plugins js-->
        <script src="{{asset('admin/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-knob/jquery.knob.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.time.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.selection.js')}}"></script>
        <script src="{{asset('admin/assets/libs/flot-charts/jquery.flot.crosshair.js')}}"></script>

         <!-- third party data table js -->
        <script src="{{asset('admin/assets/libs/datatables/jquery.dataTables.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/buttons.flash.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/buttons.print.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/datatables/dataTables.select.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/pdfmake/vfs_fonts.js')}}"></script>
        <!-- third party data table js ends -->

        <!-- select option link here-->
        <script src="{{asset('admin/assets/libs/jquery-nice-select/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/switchery/switchery.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/multiselect/jquery.multi-select.js')}}"></script>
        <script src="{{asset('admin/assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/jquery-mockjax/jquery.mockjax.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/autocomplete/jquery.autocomplete.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <!-- Init js-->
        <!-- <script src="{{asset('admin/assets/js/pages/form-advanced.init.js')}}"></script> -->
        <!-- select option link end here-->

      <!-- Plugins css  for image upload file-->

       <!-- Sweet Alerts js -->
        <script src="{{asset('admin/assets/libs/sweetalert2/sweetalert.min.js')}}"></script>

        <!-- Sweet alert init js-->
        
        <script src="{{asset('admin/assets/js/pages/sweet-alerts.init.js')}}"></script>

        <!-- Sweet Alerts js -->

     <!-- Plugins js -->
        <script src="{{asset('admin/assets/libs/dropzone/dropzone.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/dropify/dropify.min.js')}}"></script>

         <!-- Init js-->
        <script src="{{asset('admin/assets/js/pages/form-fileuploads.init.js')}}"></script>

         <!-- Date Plugin-->
         <!-- Plugins js-->
        <script src="{{asset('admin/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
        <script src="{{asset('admin/assets/libs/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
         <!-- Init js-->
        <script src="{{asset('admin/assets/js/pages/form-pickers.init.js')}}"></script>

 <!-- Plugins css  for image upload file-->
        <!-- Datatables init -->
        <script src="{{asset('admin/assets/js/pages/datatables.init.js')}}"></script>

        <!-- Dashboar 1 init js-->
        <script src="{{asset('admin/assets/js/pages/dashboard-1.init.js')}}"></script>

   <!-- Plugin js-->
        <script src="{{asset('admin/assets/libs/parsleyjs/parsley.min.js')}}"></script>
        <!-- Validation init js-->
        <script src="{{asset('admin/assets/js/pages/form-validation.init.js')}}"></script>
        <!-- App js-->
        <script src="{{asset('admin/assets/js/app.min.js')}}"></script>
         
    </body>
</html>