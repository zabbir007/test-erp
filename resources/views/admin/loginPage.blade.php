@extends('layouts.admin')

@section('title') Admin Login @endsection

@section('content')
        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card bg-pattern">

                            <div class="card-body p-4">
                                
                                <div class="text-center w-75 m-auto">
                                    <a href="index.html">
                                        <span><img src="{{asset('admin/assets/images/logo-light.png')}}" alt="" height="22"></span>
                                    </a>
                                    
                                </div>

                                <form action="{{route('check_login')}}" method="post" class="parsley-examples">

                                    <br/>
                                    <?php 
                                        $message=Session::get('message');
                                        if($message){

                                            ?>
                                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <?php
                                                    echo $message;
                                                    Session::put('message','');
                                                ?>
                                            </div>
                                            <?php
                                        
                                    }
                                    ?> 
                                    @if($errors->any())
                                    
                                    <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                                       

                                               <ul>
                                                   @foreach($errors->all() as $error)
                                                        <li>{{$error}}</li>
                                                   @endforeach
                                               </ul>
                                           
                                       
                                    </div>
                                     @endif
                                    
                                        @csrf
                                        <div class="form-group">
                                            <label>Email</label>
                                            <div>
                                                <input type="email" name="email" class="form-control parsley-validated" required
                                                        data-parsley-required-message="Please Enter Your Email"  data-parsley-type-message="Please Provide Valid Email"  placeholder="Email"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Password</label>
                                            <div>
                                                <input type="password" name="password" required class="form-control parsley-validated" 
                                                       data-parsley-required-message="Please Enter Your Password" placeholder="Password"/>
                                            </div>
                                        </div>

                                        <div class="form-group mb-0 text-center">
                                            <div>
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                    Sign In
                                                </button>
                                                <!-- <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                    Cancel
                                                </button> -->
                                            </div>
                                        </div>
                                </form>
                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
                    <!-- end container -->
        </div>
        <!-- end page -->
@endsection