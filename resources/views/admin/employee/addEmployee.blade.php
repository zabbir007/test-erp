@extends('layouts.admin')

@section('title') Add Employee @endsection

@section('content')


<div class="row">
    <div class="col-lg-6">

        <div class="card-box">
            <h4 class="header-title m-t-0">Add New Employee</h4>
            <p class="text-muted font-14 m-b-20">
                Admin Create Employee Profile and this profile access any employee.
            </p>

            <form action="{{route('insertEmployeeData')}}" class="parsley-examples" method="post">
                @csrf
                <br/>
                <?php 
                    $message=Session::get('message');
                   if($message){
                ?>
                        <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                <?php
                    
                	}
                ?> 
                <?php 
                    $messageWarning=Session::get('messageWarning');
                   if($messageWarning){
                ?>
                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                <?php
                    
                	}
                ?> 
                @if($errors->any())
	                <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
	                   

	                           <ul>
	                               @foreach($errors->all() as $error)
	                                    <li>{{$error}}</li>
	                               @endforeach
	                           </ul>
	                       
	                   
	                </div>
                @endif
                <div class="form-group">
                    <label for="emailAddress">Email address<span class="text-danger">*</span></label>
                    <input type="email" name="employee_email" parsley-trigger="change" required
                           placeholder="Enter email" class="form-control" id="emailAddress">
                </div>
                <div class="form-group">
                    <label for="pass1">Password<span class="text-danger">*</span></label>
                    <input id="pass1" type="password" placeholder="Password" required
                           class="form-control" name="password">
                </div>
                <div class="form-group">
                    <label for="passWord2">Confirm Password <span class="text-danger">*</span></label>
                    <input data-parsley-equalto="#pass1" type="password" required
                           placeholder="Password" class="form-control" id="passWord2" name="password_confirmation">
                </div>
                <div class="form-group">
                    <label for="userName">Salary<span class="text-danger">*</span></label>
                    <input type="number" name="employee_salary" parsley-trigger="change" required
                           placeholder="Enter Salary" class="form-control" id="userName">
                </div>

                <div class="form-group text-right m-b-0">
                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                        Submit
                    </button>
                </div>

            </form>
        </div> <!-- end card-box -->
    </div>
    <!-- end col -->
</div>

@endsection