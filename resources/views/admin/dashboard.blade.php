@extends('layouts.admin')

@section('title') Dashboard @endsection

@section('content')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> 
<script type="text/javascript">  
   google.charts.load('current', {'packages':['corechart']});  
   google.charts.setOnLoadCallback(drawChart);  
   function drawChart()  
   {  
        var data = google.visualization.arrayToDataTable([  
                  ['Status', 'Number'],  
                  <?php  
                  foreach($attendancce_data as $attend){  
                       echo "['".$attend->status."', ".$attend->number."],";  
                  }  
                  ?>  
             ]);  
        var options = {  
              title: 'Percentage of Present and Absence Employee This Month',  
              pieHole: 0.4  
             };  
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));  
        chart.draw(data, options);  
   }  
   </script> <script type="text/javascript">  
   google.charts.load('current', {'packages':['corechart']});  
   google.charts.setOnLoadCallback(drawChart);  
   function drawChart()  
   {  
        var data = google.visualization.arrayToDataTable([  
                  ['Status', 'Number'],  
                  <?php  
                  foreach($attendancce_data as $attend){  
                       echo "['".$attend->status."', ".$attend->number."],";  
                  } 
                  ?>  
             ]);  
        var options = {  
              title: 'Percentage of Present and Absence Employee This Month',   
              pieHole: 0.4  
             };  
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));  
        chart.draw(data, options);  
   }  
</script> 
<div class="row mt-3">
    <div class="col-lg-6">

        <div class="card-box">
        	<h4 class="header-title">All Employee List</h4>
            <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Salary</th>
                        <th>Action</th>
                    </tr>
                </thead>
            
            
                <tbody>
                	@foreach($employee_info as $single_employee_info)
                    <tr>
                        <td>{{$single_employee_info->employee_email}}</td>
                        <td>{{$single_employee_info->employee_salary}}</td>
                        <td><a href="javascript:void(0);" class="action-icon"><i class="mdi mdi-eye" data-toggle="modal" data-target="#exampleModal{{$single_employee_info->id}}"></i></a></td>
                    </tr>
                    <div class="modal fade" id="exampleModal{{$single_employee_info->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Single Employee Details</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        <div class="text-left mt-3">
                                <p class="text-muted mb-2 font-13"><strong>Employee Email :</strong> <span class="ml-2">{{$single_employee_info->employee_email}}</span></p>

                                <p class="text-muted mb-2 font-13"><strong>Employee Salary :</strong><span class="ml-2">{{$single_employee_info->employee_salary}}</span></p>

                                <p class="text-muted mb-2 font-13"><strong>Employee DOB :</strong> <span class="ml-2 "><?php if($single_employee_info->employee_dob=='0'){echo "Not Inserted";}else{echo $single_employee_info->employee_dob;} ?></span></p>

                                <p class="text-muted mb-1 font-13"><strong>Employee Designation :</strong> <span class="ml-2"><?php if($single_employee_info->employee_designation=='0'){echo "Not Inserted";}elseif($single_employee_info->employee_designation=='1'){echo "Back-End";}elseif($single_employee_info->employee_designation=='2'){echo "Front-End";}elseif($single_employee_info->employee_designation=='3'){echo "Designer";}elseif($single_employee_info->employee_designation=='4'){echo "Marketing";} ?></span></p>
                            </div>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					      </div>
					    </div>
					  </div>
					</div>
					@endforeach
                </tbody>
            </table> 
        </div> <!-- end card-box -->
    </div>

    <div class="col-lg-6">

        <div class="card-box">
           <div id="piechart" style="width: 500px; height: 500px;"></div>
        </div> <!-- end card-box -->
    </div>
    <!-- end col -->
</div>

@endsection