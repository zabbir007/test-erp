@extends('layouts.employee')

@section('title') Dashboard @endsection

@section('content')

<div class="row mt-3">
    <div class="col-lg-6">

        <div class="card-box">
        	<h4 class="header-title">This Month Attendance List</h4>
            <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
            
            
                <tbody>
                	@foreach($attendance_info as $attendance)
                    <tr>
                        <td>{{$attendance->attend_date}}</td>
                        <td><?php if($attendance->status=='1'){echo "Present";}else{echo "Absence";} ?></td>
                    </tr>
					@endforeach
                </tbody>
            </table> 
        </div> <!-- end card-box -->
    </div>

    <div class="col-lg-6">
        <div class="card-box">
            <form class="needs-validation" method="post" action="{{route('updateEmployeeInformation')}}" novalidate>
            @csrf
            <?php 
                $message=Session::get('message');
               if($message){
            ?>
                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                            echo $message;
                            Session::put('message','');
                        ?>
                    </div>
            <?php
                
            	}
            ?> 
                <div class="form-group mb-3">
                    <label for="heard">Designation:</label>
                    <select id="heard" class="form-control" name="employee_designation">
                        <option value="">Choose..</option>
                        @foreach($department_info as $department)
                        <option value="{{$department->department_code}}" <?php if($department->department_code==$employee_info->employee_designation){echo "selected";} ?> >{{$department->department_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label for="">DOB</label>
                    <input type="date" class="form-control" id="" name="employee_dob" placeholder="DOB" value="<?php if($employee_info->employee_dob=='0'){echo date('Y-m-d');}else{echo $employee_info->employee_dob;} ?>">
                </div>
                <div class="form-group mb-3">
                    <label for="">Salary</label>
                    <input type="text" class="form-control" id="" placeholder="Salary" value="{{$employee_info->employee_salary}}" readonly="">
                </div>
                <div class="form-group row">
                    <div class="col-8 offset-4">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div> <!-- end card-box -->
    </div>
    <!-- end col -->
</div>

@endsection