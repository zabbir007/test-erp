<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
date_default_timezone_set('Asia/Dhaka');
class SuperAdminController extends Controller
{
    public function __construct(){
         
        $this->middleware('checkAdmin');
    }
    //show dashboard
    public function superAdminDashboard(){
        $currentDate=date('Y-m-d');
        $year = date('Y', strtotime($currentDate));
        $month = date('m', strtotime($currentDate));
        $employee_info=DB::table('employees')
                            ->get();
        $department_info=DB::table('departments')
                            ->get();
        $attendancce_data = DB::table('attendances')
                                ->select(DB::raw('count(*) as number, status'))
                                ->groupBy('status')
                                ->whereMonth('attend_date',$month)
                                ->whereYear('attend_date',$year)
                                ->get();

        // echo "<pre/>";
        // print_r($attendancce_data);
        // exit();
    	return view('admin.dashboard',compact('employee_info','department_info','attendancce_data'));
    }
    //admin logout
    public function superAdminLogout(){
    	Session::put('adminId','');
        return redirect()->route('login_page');
    }
    //admin employee portion
    public function addEmployee(){
        return view('admin.employee.addEmployee');
    }

    public function insertEmployeeData(Request $request){
        //laravel from validation
        $request->validate([
            'employee_email' => 'required',
            'employee_salary' => 'required',
            'password' => 'required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'required',
        ]);
        //email check
        $checkEmail=DB::table('employees')
                        ->get();
        foreach($checkEmail as $checkE){
            if ($checkE->employee_email ==$request->employee_email) {
                Session::put('messageWarning','This Email Allready Used!!');
                return redirect()->back();
            }
        }
        //get data for array
        $data=array();
        $data['employee_email']=$request->employee_email;
        $data['employee_salary']=$request->employee_salary;
        $data['employee_password']=md5($request->password);
        $data['created_at']=date('Y-m-d');
        $data['updated_at']=date('Y-m-d');
        $insertEmployeeData=DB::table('employees')
                                ->insert($data);
        if ($insertEmployeeData){
            Session::put('message','Employee Create Successfully!!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Employee Create Failed!!!!!!');
            return redirect()->back();
        }
    }
}
