<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
date_default_timezone_set('Asia/Dhaka');
class SuperEmployeeController extends Controller
{
    public function __construct(){
         
        $this->middleware('checkEmployee');
    }
    //show dashboard
    public function superEmployeeDashboard(){
        $employee_id=Session::get('employeeId');
        $department_info=DB::table('departments')
                            ->get();
        $employee_info=DB::table('employees')
                            ->where('id',$employee_id)
                            ->first();
        $currentDate=date('Y-m-d');
        $year = date('Y', strtotime($currentDate));
        $month = date('m', strtotime($currentDate));
        $attendance_info = DB::table('attendances')
                                ->whereMonth('attend_date',$month)
                                ->whereYear('attend_date',$year)
                                ->where('employee_id',$employee_id)
                                ->orderBy('attendances.id', 'DESC')
                                ->get();
        // echo "<pre/>";
        // print_r($attendance);
        // exit();
    	return view('employee.employeeDashboard',compact('department_info','attendance_info','employee_info'));
    }
    //employee logout
    public function superEmployeeLogout(){
    	Session::put('employeeId','');
        return redirect()->route('employee_login_page');
    }
    //update information
    public function updateEmployeeInformation(Request $request){
        $data=array();
        $employee_id=Session::get('employeeId');
        $employee_info=DB::table('employees')
                            ->where('id',$employee_id)
                            ->first();
        if($request->employee_dob !=null){
            $data['employee_dob']=$request->employee_dob;
        }else{
           
            $data['employee_dob']=$employee_info->employee_dob;
        }
        if($request->employee_designation !=null){
            $data['employee_designation']=$request->employee_designation;
        }else{
           
            $data['employee_designation']=$employee_info->employee_designation;
        }
        DB::table('employees')
            ->where('id', $employee_id)
            ->update($data);
        Session::put('message','Your Profile Update Successfully!!');
        return redirect()->back();
    }
    //attendance portion
    public function employeeAttendance(){
        $date=date('Y-m-d');
        $attend_info=DB::table('attendances')
                            ->select('attendances.status')
                            ->where('attend_date','=',$date)
                            ->first();
        // echo "<pre/>";
        // print_r($attend_info);
        // exit();
        return view('employee.addAttendance',compact('attend_info'));
    }

    public function insertEmployeeAttendance(Request $request){
        $data=array();
        $employee_id=Session::get('employeeId');
        $request->validate([
            'status' => 'required',
        ]);
        $date=date('Y-m-d');
        $attend_info=DB::table('attendances')
                            ->select('attendances.status')
                            ->where('attend_date','=',$date)
                            ->first();
        if ($attend_info) {
           $data['status']=$request->status;
           $update_attend=DB::table('attendances')
                            ->where('attend_date','=',$date)
                            ->update($data);
            Session::put('message','Attendance Update Done !!');
            return redirect()->back();
        }else{
            $data['status']=$request->status;
            $data['attend_date']=$date;
            $data['employee_id']=$employee_id;
            $insert_attend=DB::table('attendances')
                            ->insert($data);
            Session::put('message','Attendance Sunbit Done !!');
            return redirect()->back();
        }
    }
}
